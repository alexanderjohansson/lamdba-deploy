# Create user in [IAM](https://console.aws.amazon.com/iam/home)

`AdministratorAccess`

## Export vars

```sh
export AWS_ACCESS_KEY_ID=xxx
export AWS_SECRET_ACCESS_KEY=xxx
```

# Add `serverless` to project

## Install

```
npm install --save-dev serverless serverless-offline
npm install serverless-http
```

## `package.json`:

```json
"deploy-staging": "serverless deploy --stage staging",
"deploy-production": "serverless deploy --stage production",
"sls": "sls offline start",
```

# Add `/bitbucket-pipelines.yml`

```yml
# This is a sample build configuration for JavaScript.
# Check our guides at https://confluence.atlassian.com/x/14UWN for more examples.
# Only use spaces to indent your .yml configuration.
# -----
# You can specify a custom docker image from Docker Hub as your build environment.
image: node:8.10.0

pipelines:
  default:
    - step:
        caches:
          - node
        script: # Modify the commands below to build your repository.
          - npm install
  branches:
    master:
      - step:
          deployment: production
          script:
            - npm install
            - npm run build
            - npm run deploy-production
    develop:
      - step:
          deployment: staging
          script:
            - npm install
            - npm run build
            - npm run deploy-staging
```

# Add `/serverless.yml`

Update service name below

```yml
service: your-service

provider:
  name: aws
  runtime: nodejs8.10
  region: eu-central-1

functions:
  app:
    handler: handler.ssr
    events:
      - http: ANY /
      - http: 'ANY {proxy+}'
plugins:
  - serverless-offline
```

# Add `/handler.js`

We export a function called `ssr` that includes the whole app

```js
const ssr = require('./build-server/server').default;

module.exports.hello = async (event, context) => {
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: 'Go Serverless v1.0! Your function executed successfully!',
      input: event,
    }),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};

module.exports.ssr = ssr;
```

## Update your app to work with `serverless`

See [`./src/server.js`](./src/server.js)

```js
import serverless from 'serverless-http';
// ...
export default serverless(app);
```

# Test locally

```sh
npm run sls
```

# Test deploying

```sh
npm run deploy-staging
```

If this works, deploy to Bitbucket and it should all be good

# Custom domain for deploys

```
now dns add kattcorp.com _eae350e16352a07792a0f0e0c98e8a4a.lamdba-nodejs CNAME _c5a8669c00f0278e482b3850fd398626.tljzshvwok.acm-validations.aws.
now dns add kattcorp.com '@' CAA '0 issue "amazon.com"'
```

## Setup existding domain in AWS

1. Go https://console.aws.amazon.com/acm/home?region=us-east-1#/ and issue SSL certificate for your subdomain
1. Go to DNS Management under Route 53 - https://console.aws.amazon.com/route53/home?region=eu-west-3#hosted-zones:
1. Create hosted zone for your domain `example.com`

```
yarn add serverless-domain-manager --dev
```

MAKE SURE YOU ARE IN THE RIGHT REGION
