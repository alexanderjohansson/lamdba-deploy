import serverless from 'serverless-http';
import express from 'express';
import cors from 'cors';
import path from 'path';

const app = express();

app.use(cors());
app.use(
  express.static(path.resolve(__dirname, '..', 'build'), { index: false }),
);

app.get('*', (req, res) => {
  // rehydrate, etc etc
  res.send('hello');
});

export default serverless(app);
